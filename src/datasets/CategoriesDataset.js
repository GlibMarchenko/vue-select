export default [
    {
        label: 'Business',
        children: [
            {
                label: 'Sales',
                children: [
                    {
                        label: 'Automotive',
                    },
                    {
                        label: 'Computers',
                    },
                    {
                        label: 'Other',
                    }
                ],
            },
            {
                label: 'Industry',
                children: [
                    {
                        label: 'Gardening',
                    },
                    {
                        label: 'Photography',
                    },
                    {
                        label: 'Other',
                    }
                ],
            }
        ],
    },
    {
        label: 'Exploration',
        children: [
            {
                label: 'UIUX',
                children: [
                    {
                        label: 'Books',
                    },
                    {
                        label: 'Computers',
                    },
                    {
                        label: 'Other',
                    }
                ],
            },
            {
                label: 'Travel',
                children: [
                    {
                        label: 'Relocation',
                    },
                    {
                        label: 'Photography',
                    },
                    {
                        label: 'Other',
                    }
                ],
            }
        ],
    },

]
